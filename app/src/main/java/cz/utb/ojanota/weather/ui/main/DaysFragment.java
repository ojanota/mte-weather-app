package cz.utb.ojanota.weather.ui.main;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import cz.utb.ojanota.weather.MainActivity;
import cz.utb.ojanota.weather.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class DaysFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String STORED_DAYS = "days";
    private static final String LAST_UPDATE = "last_update_days_city";
    private static final long UPDATE_DIFF = 60*60*1000;

    private Pair selectedCity;
    private JSONObject storedData;

    private PageViewModel pageViewModel;

    public static DaysFragment newInstance(int index) {
        DaysFragment fragment = new DaysFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setStoredData(final View root) {

        if (storedData != null && storedData.length() > 0){
            setDaysWeather(storedData, root);
            return;
        }

        JSONObject jsonObject = new JSONObject();

        File file = new File(getContext().getFilesDir(), STORED_DAYS + selectedCity.first);
        if (!file.exists()) {
            try {
                file.createNewFile();
                try (FileOutputStream fos = getContext().openFileOutput(STORED_DAYS + selectedCity.first, Context.MODE_PRIVATE)) {
                    fos.write(("{}").getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream fis;
        try {
            fis = Objects.requireNonNull(getContext()).openFileInput(STORED_DAYS + selectedCity.first);
            InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8);

            StringBuilder stringBuilder = new StringBuilder();

            try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                String line = reader.readLine();
                while (line != null) {
                    stringBuilder.append(line).append('\n');
                    line = reader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                String contents = stringBuilder.toString();

                JSONObject obj = new JSONObject(contents);

                if (obj.length() > 0) {
                    storedData = obj;
                    setDaysWeather(obj, root);
                }
                else {
                    reloadDataFromServer(root);
                }

                fis.close();
            }
        } catch (JSONException | IOException ex) {
            ex.printStackTrace();
        }
    }

    public void reloadDataFromServer(final View root) {
        Toast.makeText(root.getContext(), "Updating...", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = MyRequestQueue.getQueue(DaysFragment.this);

        String url = MainActivity.WEATHER_API_URL + "forecast?id=" + selectedCity.first;
        url += "&units=metric&appid=" + MainActivity.WEATHER_API_APPID;

        Log.d("API", url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.length() > 0) {
                    File file = new File(root.getContext().getFilesDir(), STORED_DAYS + selectedCity.first);
                    if (!file.exists()) {
                        try {
                            file.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    int city = 0;
                    try {
                        JSONObject cityObject = response.getJSONObject("city");
                        city = cityObject.getInt("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try (FileOutputStream fos = root.getContext().openFileOutput(STORED_DAYS + selectedCity.first, Context.MODE_PRIVATE)) {
                        fos.write(response.toString().getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File last = new File(root.getContext().getFilesDir(), LAST_UPDATE + city);
                    if (!last.exists()) {
                        try {
                            last.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    try (FileOutputStream fos = root.getContext().openFileOutput(LAST_UPDATE + city, Context.MODE_PRIVATE)) {
                        JSONObject lastUpdate = new JSONObject();
                        try {
                            lastUpdate.put("date", new Date().getTime());

                            lastUpdate.put("city", city);

                            fos.write(lastUpdate.toString().getBytes());

                            fos.close();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    storedData = response;
                    setDaysWeather(response, root);
                    Toast.makeText(root.getContext(), "Data updated successfully.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(root.getContext(), "During data updating went something wrong.", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(root.getContext(), "During data updating went something wrong.", Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY ERROR", error.getMessage());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }


    private TextView getNewTextView(Context context, String text, int width) {
        Resources resources = context.getResources();

        int leftMargin = context.getResources().getDimensionPixelSize(R.dimen.margin16dp);
        int topMargin = context.getResources().getDimensionPixelSize(R.dimen.margin8dp);
        int rightMargin = context.getResources().getDimensionPixelSize(R.dimen.margin16dp);
        int bottomMargin = 0;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);

        TextView textView = new TextView(context);
        textView.setLayoutParams(params);
        textView.setTextColor(resources.getColor(android.R.color.white));
        textView.setText(text);
        return textView;
    }

    private void setDaysWeather(JSONObject obj, View root) {
        if (obj.length() > 0) {
            try {
                JSONArray weather = obj.getJSONArray("list");

                String lastDate = null;

                int layoutMargin = root.getResources().getDimensionPixelSize(R.dimen.margin16dp);

                Context context = root.getContext();
                LinearLayout daysBaseLayout = root.findViewById(R.id.daysBaseLayout);
                LinearLayout linearLayout = null;
                for (int i = 0; i < weather.length(); i++) {
                    JSONObject part = weather.getJSONObject(i);

                    long timestamp = part.getLong("dt") - 60*60;
                    String dateString = TodayFragment.getDateTime(timestamp, "dd.MM");

                    if (lastDate == null || !lastDate.equals(dateString)) {
                        TextView text = getNewTextView(context, dateString, ViewGroup.LayoutParams.MATCH_PARENT);
                        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        daysBaseLayout.addView(text);
                        lastDate = dateString;

                        HorizontalScrollView.LayoutParams horLayoutParams = new HorizontalScrollView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        horLayoutParams.setMargins(0,0,layoutMargin,layoutMargin);
                        final HorizontalScrollView horizontalScrollView = new HorizontalScrollView(context);
                        horizontalScrollView.setLayoutParams(horLayoutParams);
                        daysBaseLayout.addView(horizontalScrollView);

                        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        linearLayoutParams.setMargins(0,0,0,layoutMargin/2);
                        linearLayout = new LinearLayout(context);
                        linearLayout.setLayoutParams(linearLayoutParams);
                        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        horizontalScrollView.addView(linearLayout);
                    }
                    setPartWeather(part, root, linearLayout);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setPartWeather(JSONObject part, View root, LinearLayout linearLayout) {
        Context context = root.getContext();

        final float density = context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT;
        int layoutMargin = root.getResources().getDimensionPixelSize(R.dimen.margin16dp);

        LinearLayout.LayoutParams linLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        linLayoutParams.setMargins(layoutMargin, layoutMargin, 0, layoutMargin);
        final LinearLayout linLayout = new LinearLayout(context);
        linLayout.setPadding(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
        linLayout.setOrientation(LinearLayout.VERTICAL);
        linLayout.setLayoutParams(linLayoutParams);
        linLayout.setBackground(root.getResources().getDrawable(R.drawable.days_border));
        linearLayout.addView(linLayout);

        int white = root.getResources().getColor(android.R.color.white);


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(16,8,16,8);

        TextView text;

        try {
            long timestamp = part.getLong("dt") - 60*60;

            text = getNewTextView(context, TodayFragment.getDateTime(timestamp, "HH:mm"), ViewGroup.LayoutParams.MATCH_PARENT);
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            linLayout.addView(text);

            LinearLayout.LayoutParams dividerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);

            View divider = new View(context);
            divider.setLayoutParams(dividerParams);
            divider.setBackgroundColor(white);
            linLayout.addView(divider);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject main = null;
        try {
            main = part.getJSONObject("main");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int pixels = (int) (130 * density + 0.5f);

        LinearLayout.LayoutParams pars = new LinearLayout.LayoutParams(pixels, ViewGroup.LayoutParams.WRAP_CONTENT);
        pars.setMargins(16, 8, 16, 8);

        LinearLayout row;

        if (main != null) {
            try {
                double temp = main.getDouble("temp");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Temperature", pixels);
                row.addView(text);

                text = getNewTextView(context, Math.round(temp) + "°C", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                double realFeel = main.getDouble("feels_like");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "RealFeel:", pixels);
                row.addView(text);

                text = getNewTextView(context, Math.round(realFeel) + "°C", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                int humidity = main.getInt("humidity");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Humidity:", pixels);
                row.addView(text);

                text = getNewTextView(context, humidity + "%", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                int pressure = main.getInt("pressure");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Pressure:", pixels);
                row.addView(text);

                text = getNewTextView(context, pressure + " hPa", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONArray weather = null;
        JSONObject actual = null;
        try {
            weather = part.getJSONArray("weather");

            actual = weather.getJSONObject(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (actual != null) {
            try {
                String desc = actual.getString("description");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Description:", pixels);
                row.addView(text);

                text = getNewTextView(context, desc, ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            JSONObject clouds = part.getJSONObject("clouds");

            int all = clouds.getInt("all");

            row = getNewRowLayout(context);
            linLayout.addView(row);

            text = getNewTextView(context, "Cloudiness:", pixels);
            row.addView(text);

            text = getNewTextView(context, all + "%", ViewGroup.LayoutParams.MATCH_PARENT);
            row.addView(text);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject wind = null;

        try {
            wind = part.getJSONObject("wind");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (wind != null) {

            try {
                double speed = wind.getDouble("speed");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Wind speed:", pixels);
                row.addView(text);

                text = getNewTextView(context, speed + " m/s", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                int degrees = wind.getInt("deg");
                String direction = TodayFragment.getWindDirection(degrees);

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Wind direction:", pixels);
                row.addView(text);

                text = getNewTextView(context, direction, ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        JSONObject rain = null;

        try {
            rain = part.getJSONObject("rain");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (rain != null) {
            try {
                double rainHour = rain.getDouble("1h");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Rain volume 1 hour:", pixels);
                row.addView(text);

                text = getNewTextView(context, rainHour + " mm", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                double rainHour = rain.getDouble("3h");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Rain volume 3 hours:", pixels);
                row.addView(text);

                text = getNewTextView(context, rainHour + " mm", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        JSONObject snow = null;

        try {
            snow = part.getJSONObject("snow");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (snow != null) {
            try {
                double snowHour = snow.getDouble("1h");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Snow volume 1 hour:", pixels);
                row.addView(text);

                text = getNewTextView(context, snowHour + " mm", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                double snowHour = snow.getDouble("3h");

                row = getNewRowLayout(context);
                linLayout.addView(row);

                text = getNewTextView(context, "Snow volume 3 hours:", pixels);
                row.addView(text);

                text = getNewTextView(context, snowHour + " mm", ViewGroup.LayoutParams.MATCH_PARENT);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private LinearLayout getNewRowLayout(Context context) {
        LinearLayout row = new LinearLayout(context);
        LinearLayout.LayoutParams tableRowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(tableRowParams);
        row.setOrientation(LinearLayout.HORIZONTAL);
        return row;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    private void getSelectedCity(View root) {
        CityFragment cityFragment = CityFragment.newInstance(1);
        cityFragment.setSelectedCity(root);
        selectedCity = cityFragment.getSelectedCity();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_days, container, false);

        setCity(root);

        setStoredData(root);

        controlUpdate(root);

        handleUpdate(root);

        return root;
    }

    private void setCity(View root) {
        getSelectedCity(root);

        TextView city = root.findViewById(R.id.city);

        city.setText(selectedCity.second.toString());
    }

    private void controlUpdate(View root) {
        long time = new Date().getTime();
        File file = new File(root.getContext().getFilesDir(), LAST_UPDATE + selectedCity.first);
        if (!file.exists()) {
            reloadDataFromServer(root);
        } else {
            FileInputStream fis;
            try {
                fis = root.getContext().openFileInput(LAST_UPDATE + selectedCity.first);
                InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8);

                StringBuilder stringBuilder = new StringBuilder();

                try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                    String line = reader.readLine();
                    while (line != null) {
                        stringBuilder.append(line).append('\n');
                        line = reader.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    String contents = stringBuilder.toString();

                    JSONObject obj = new JSONObject(contents);

                    if (obj.length() > 0) {
                        int city = obj.getInt("city");
                        time = obj.getLong("date");
                        long now = (new Date()).getTime();

                        if (city != (int)selectedCity.first || now - time > UPDATE_DIFF) {
                            reloadDataFromServer(root);
                            time = new Date().getTime();
                        }
                    }
                    else {
                        reloadDataFromServer(root);
                    }

                    fis.close();
                }
            } catch (JSONException e) {
                reloadDataFromServer(root);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        TextView update = root.findViewById(R.id.updateText);
        String date = DateFormat.format("dd.MM.yyyy HH:mm", new Date(time)).toString();
        String updateText = "Last updated: " + date;
        update.setText(updateText);
    }

    private void handleUpdate(final View root) {
        ImageButton update = root.findViewById(R.id.update);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reloadDataFromServer(root);
                long time = new Date().getTime();
                TextView update = root.findViewById(R.id.updateText);
                String date = DateFormat.format("dd.MM.yyyy HH:mm", new Date(time)).toString();
                String updateText = "Last updated: " + date;
                update.setText(updateText);
            }
        });
    }
}