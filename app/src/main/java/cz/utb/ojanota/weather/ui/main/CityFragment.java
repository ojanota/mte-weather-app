package cz.utb.ojanota.weather.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import cz.utb.ojanota.weather.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class CityFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;
    private ArrayList<Pair> data = null;

    private Pair selectedCity;

    private static final String MY_CITIES = "my_cities";
    private static final String SELECTED_CITY = "selected_city";

    public static CityFragment newInstance(int index) {
        CityFragment fragment = new CityFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    private Pair findCity(String city) {
        for (Pair item : data) {
            if (item.second.equals(city)) {
                return item;
            }
        }

        return null;
    }

    private void selectCity(Pair city, View root) {
        selectedCity = city;
        TextView cityName = root.findViewById(R.id.cityName);

        if (cityName != null) {
            cityName.setText(selectedCity.second.toString());

            File file = new File(getContext().getFilesDir(), SELECTED_CITY);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try (FileOutputStream fos = getContext().openFileOutput(SELECTED_CITY, Context.MODE_PRIVATE)) {
                fos.write(("{id: " + city.first + ", name: \"" + city.second + "\"}").getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setSearchData() {
        if (this.data != null && !this.data.isEmpty()) {
            return;
        }

        Resources resources = getResources();
        String packageName = this.getActivity().getPackageName();

        String json = null;

        ArrayList<Pair> formList = new ArrayList<>();

        try {
            try {
                InputStream cities = resources.openRawResource(resources.getIdentifier("city_list_cz_sk", "raw", packageName));
                int size = cities.available();
                byte[] buffer = new byte[size];
                cities.read(buffer);
                cities.close();
                json = new String(buffer, "UTF-8");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            JSONArray array = new JSONArray(json);
            Pair pair;

            for (int i = 0; i < array.length(); i++) {
                JSONObject jo = array.getJSONObject(i);
                Integer id = jo.getInt("id");
                String name = jo.getString("name");

                pair = new Pair(id, name);

                formList.add(pair);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.data = formList;
    }

    void setSelectedCity(View root) {
        File file = new File(root.getContext().getFilesDir(), SELECTED_CITY);
        if (!file.exists()) {
            try {
                file.createNewFile();
                try (FileOutputStream fos = root.getContext().openFileOutput(SELECTED_CITY, Context.MODE_PRIVATE)) {
                    fos.write("{id: 3078610, name: \"Brno\"}".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream fis;
        try {
            fis = Objects.requireNonNull(root.getContext()).openFileInput(SELECTED_CITY);
            InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8);

            StringBuilder stringBuilder = new StringBuilder();

            try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                String line = reader.readLine();
                while (line != null) {
                    stringBuilder.append(line).append('\n');
                    line = reader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                String contents = stringBuilder.toString();

                JSONObject obj = new JSONObject(contents);
                selectCity(new Pair(obj.get("id"), obj.get("name")), root);

                fis.close();
            }
        } catch (JSONException | IOException ex) {
            ex.printStackTrace();
        }
    }

    private JSONArray getSavedCities(View root) {
        File directory = Objects.requireNonNull(root.getContext()).getFilesDir();
        File file = new File(directory, MY_CITIES);
        if (!file.exists()) {
            try {
                file.createNewFile();
                try (FileOutputStream fos = root.getContext().openFileOutput(MY_CITIES, Context.MODE_PRIVATE)) {
                    fos.write("[]\n".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            FileInputStream fis = root.getContext().openFileInput(MY_CITIES);
            InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8);

            StringBuilder stringBuilder = new StringBuilder();

            String contents;

            try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                String line = reader.readLine();
                while (line != null) {
                    stringBuilder.append(line).append('\n');
                    line = reader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                contents = stringBuilder.toString();
            }

            return new JSONArray(contents);
        } catch (FileNotFoundException | JSONException e) {
            e.printStackTrace();
        }

        return new JSONArray();
    }

    private void setEarlierCities(JSONArray savedJson, final View root) {
        final ListView list = root.findViewById(R.id.listView);

        if (savedJson.length() > 0) {
            List<String> earlier = new ArrayList<>();
            for (int i = 0; i < savedJson.length(); i++) {
                try {
                    JSONObject obj = (JSONObject) savedJson.get(i);
                    if (data != null) {
                        earlier.add(obj.get("name").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            ArrayAdapter arrayAdapter = new ArrayAdapter<>(root.getContext(), R.layout.list_item, earlier);
            list.setAdapter(arrayAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Pair city = findCity(adapterView.getAdapter().getItem(i).toString());

                    if (city != null) {
                        root.clearFocus();
                        selectCity(city, root);

                        Toast.makeText(getContext(), "You selected " +  city.second.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void handleSearching(JSONArray savedJson, final View root) {
        final SearchView searchView = root.findViewById(R.id.searchView);
        searchView.setBackgroundResource(R.drawable.city_background);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                searchView.setIconified(false);
            }
        });

        final Context context = root.getContext();

        final JSONArray finalSavedJson = savedJson;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                if (s.isEmpty()) {
                    return false;
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.isEmpty()) {
                    return false;
                }

                List<String> array = new ArrayList<>();

                ListView list = root.findViewById(R.id.listView);

                if (data != null) {

                    for (Pair item : data) {
                        if (item.second.toString().toLowerCase().contains(s.toLowerCase())) {
                            array.add(item.second.toString());
                        }
                    }

                    ArrayAdapter arrayAdapter = new ArrayAdapter<>(context, R.layout.list_item, array);
                    list.setAdapter(arrayAdapter);

                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            String selected = adapterView.getAdapter().getItem(i).toString();

                            Pair city = findCity(selected);

                            if (city != null) {
                                try (FileOutputStream fos = context.openFileOutput(MY_CITIES, Context.MODE_PRIVATE)) {
                                    boolean duplicate = false;
                                    for (int index = 0; index < finalSavedJson.length(); index++) {
                                        JSONObject obj = finalSavedJson.getJSONObject(index);
                                        if (obj.get("id").equals(city.first)) {
                                            duplicate = true;
                                            break;
                                        }
                                    }

                                    if (!duplicate) {
                                        finalSavedJson.put(new JSONObject("{id: " + city.first + ", name: \"" + city.second + "\"}"));

                                        if (finalSavedJson.length() > 5) {
                                            finalSavedJson.remove(0);
                                        }
                                        fos.write(finalSavedJson.toString().getBytes());
                                    }
                                } catch (IOException | JSONException e) {
                                    e.printStackTrace();
                                }

                                selectCity(city, root);
                                Toast.makeText(context, "You selected " +  city.second.toString(), Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(context, "Selected city was not found", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }
                return false;
            }
        });
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_city, container, false);

        this.setSearchData();

        setSelectedCity(root);

        JSONArray savedJson = getSavedCities(root);

        setEarlierCities(savedJson, root);

        handleSearching(savedJson, root);

        return root;
    }

    public Pair getSelectedCity() {
        return this.selectedCity;
    }
}