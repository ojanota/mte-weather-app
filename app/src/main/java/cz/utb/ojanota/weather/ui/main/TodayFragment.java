package cz.utb.ojanota.weather.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import cz.utb.ojanota.weather.MainActivity;
import cz.utb.ojanota.weather.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class TodayFragment extends Fragment {

    private static final String STORED_TODAY = "today";
    private static final String LAST_UPDATE = "last_update_city";

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final long UPDATE_DIFF = 15*60*1000;

    private PageViewModel pageViewModel;

    private Pair selectedCity;
    private JSONObject storedData;

    public static TodayFragment newInstance(int index) {
        TodayFragment fragment = new TodayFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    public void reloadDataFromServer(final View root) {
        Toast.makeText(root.getContext(), "Updating...", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = MyRequestQueue.getQueue(TodayFragment.this);

        String url = MainActivity.WEATHER_API_URL + "weather?id=" + selectedCity.first;
        url += "&units=metric&appid=" + MainActivity.WEATHER_API_APPID;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.length() > 0) {
                    File file = new File(root.getContext().getFilesDir(), STORED_TODAY + selectedCity.first);
                    if (!file.exists()) {
                        try {
                            file.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    int city = 0;
                    try {
                        city = response.getInt("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try (FileOutputStream fos = root.getContext().openFileOutput(STORED_TODAY + selectedCity.first, Context.MODE_PRIVATE)) {
                        fos.write(response.toString().getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    File last = new File(root.getContext().getFilesDir(), LAST_UPDATE + city);
                    if (!last.exists()) {
                        try {
                            last.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("FILE_CREATION_FAILED", e.getMessage());
                        }
                    }

                    try (FileOutputStream fos = root.getContext().openFileOutput(LAST_UPDATE + city, Context.MODE_PRIVATE)) {
                        JSONObject lastUpdate = new JSONObject();
                        try {
                            lastUpdate.put("date", new Date().getTime());

                            lastUpdate.put("city", city);

                            fos.write(lastUpdate.toString().getBytes());

                            fos.close();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    storedData = response;
                    setTodayWeather(response, root);
                    Toast.makeText(root.getContext(), "Data updated successfully.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(root.getContext(), "During data updated went something wrong.", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(root.getContext(), "During data updating went something wrong.", Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY ERROR", error.getMessage());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    private void setTodayWeather(JSONObject obj, View root) {
        if (obj.length() > 0) {
            try {
                JSONArray weather = obj.getJSONArray("weather");

                JSONObject actual = weather.getJSONObject(0);

                setMainWeather(actual, obj, root);

                setCurrentDetails(obj, root);

                setRain(obj, root);

                setSnow(obj, root);

                setWind(obj, root);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setMainWeather(JSONObject actual, JSONObject obj, View root) {
        ImageView iconImage = root.findViewById(R.id.icon);
        try {
            String icon = actual.getString("icon");
            int id = root.getContext().getResources().getIdentifier("img_" + icon, "drawable", root.getContext().getPackageName());
            iconImage.setImageResource(id);
            iconImage.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            iconImage.setVisibility(View.INVISIBLE);
        }

        TextView description = root.findViewById(R.id.description);
        try {
            String desc = actual.getString("description");
            String descriptionText = desc.substring(0,1).toUpperCase() + desc.substring(1).toLowerCase();
            description.setText(descriptionText);
            description.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            description.setVisibility(View.INVISIBLE);
        }

        JSONObject main = null;
        try {
            main = obj.getJSONObject("main");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (main != null) {
            TextView temperature = root.findViewById(R.id.temperature);
            try {
                double temp = main.getDouble("temp");
                temperature.setText(String.valueOf(Math.round(temp)));
                temperature.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                temperature.setVisibility(View.INVISIBLE);
            }

            TextView realFeel = root.findViewById(R.id.realFeel);
            try {
                double feels = main.getDouble("feels_like");
                String realFeelText = "RealFeel temperature: " + Math.round(feels) + "°C";
                realFeel.setText(realFeelText);
                realFeel.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                realFeel.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void setCurrentDetails(JSONObject obj, View root) {
        JSONObject main = null;
        try {
            main = obj.getJSONObject("main");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (main != null) {
            TextView humidity = root.findViewById(R.id.humidity);
            try {
                int hum = main.getInt("humidity");
                String humidityText = hum + "%";
                humidity.setText(humidityText);
                root.findViewById(R.id.humidityRow).setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                humidity.setVisibility(View.INVISIBLE);
                root.findViewById(R.id.humidityRow).setVisibility(View.GONE);
            }

            try {
                int press = main.getInt("pressure");
                String pressureText = press + " hPa";
                TextView pressure = root.findViewById(R.id.pressure);
                pressure.setText(pressureText);
                root.findViewById(R.id.pressureRow).setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.pressureRow).setVisibility(View.GONE);
            }
        }

        try {
            JSONObject clouds = obj.getJSONObject("clouds");
            int cloudiness = clouds.getInt("all");
            String cloudinessText = cloudiness + "%";
            TextView cloud = root.findViewById(R.id.clouds);
            cloud.setText(cloudinessText);
            root.findViewById(R.id.cloudsRow).setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            root.findViewById(R.id.cloudsRow).setVisibility(View.GONE);
            e.printStackTrace();
        }

        try {
            int visib = obj.getInt("visibility");

            String visibilityText = "";

            if (visib >= 1000) {
                visibilityText = visib / 1000 + " km";
            }
            else {
                visibilityText = visib + " m";
            }

            TextView visibility = root.findViewById(R.id.visibility);
            visibility.setText(visibilityText);
            root.findViewById(R.id.visibilityRow).setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            root.findViewById(R.id.visibilityRow).setVisibility(View.GONE);
        }

        JSONObject sys = null;
        try {
            sys = obj.getJSONObject("sys");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (sys != null) {
            try {
                long sunriseTimestamp = sys.getLong("sunrise");
                String sunriseTime = getDateTime(sunriseTimestamp, "HH:mm");
                TextView sunrise = root.findViewById(R.id.sunrise);
                sunrise.setText(sunriseTime);
                root.findViewById(R.id.sunriseRow).setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                root.findViewById(R.id.sunriseRow).setVisibility(View.GONE);
                e.printStackTrace();
            }

            try {
                long sunsetTimestamp = sys.getLong("sunset");
                String sunsetTime = getDateTime(sunsetTimestamp, "HH:mm");
                TextView sunset = root.findViewById(R.id.sunset);
                sunset.setText(sunsetTime);
                root.findViewById(R.id.sunsetRow).setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                root.findViewById(R.id.sunsetRow).setVisibility(View.GONE);
                e.printStackTrace();
            }
        }
    }

    private void setRain(JSONObject obj, View root) {
        JSONObject rain = null;
        LinearLayout rainLayout = root.findViewById(R.id.rainLayout);
        try {
            rain = obj.getJSONObject("rain");
        } catch (JSONException e) {
            e.printStackTrace();
            rainLayout.setVisibility(View.GONE);
        }

        if (rain != null) {
            boolean visible = true;
            try {
                double hourRain = rain.getDouble("1h");
                String hourRainText = hourRain + " mm";
                TextView rain1Hour = root.findViewById(R.id.rain1);
                rain1Hour.setText(hourRainText);
                root.findViewById(R.id.rain1Row).setVisibility(View.VISIBLE);
                rainLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.rain1Row).setVisibility(View.GONE);
                rainLayout.setVisibility(View.GONE);
                visible = false;
            }

            try {
                double hoursRain = rain.getDouble("3h");
                String hoursRainText = hoursRain + " mm";
                TextView rain3Hour = root.findViewById(R.id.rain3);
                rain3Hour.setText(hoursRainText);
                root.findViewById(R.id.rain3Row).setVisibility(View.VISIBLE);
                rainLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.rain3Row).setVisibility(View.GONE);
                if (!visible) {
                    rainLayout.setVisibility(View.GONE);
                }
            }
        }
    }

    private void setSnow(JSONObject obj, View root) {
        JSONObject snow = null;
        LinearLayout snowLayout = root.findViewById(R.id.snowLayout);
        try {
            snow = obj.getJSONObject("snow");
        } catch (JSONException e) {
            e.printStackTrace();
            snowLayout.setVisibility(View.GONE);
        }

        if (snow != null) {
            boolean visible = true;
            try {
                double hourSnow = snow.getDouble("1h");
                String hourSnowText = hourSnow + " mm";
                TextView snow1Hour = root.findViewById(R.id.snow1);
                snow1Hour.setText(hourSnowText);
                root.findViewById(R.id.snow1Row).setVisibility(View.VISIBLE);
                snowLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.snow1Row).setVisibility(View.GONE);
                snowLayout.setVisibility(View.GONE);
                visible = false;
            }

            try {
                double hoursSnow = snow.getDouble("3h");
                String hoursSnowText = hoursSnow + " mm";
                TextView snow3Hour = root.findViewById(R.id.snow3);
                snow3Hour.setText(hoursSnowText);
                root.findViewById(R.id.snow3Row).setVisibility(View.VISIBLE);
                snowLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.snow3Row).setVisibility(View.GONE);
                if (!visible) {
                    snowLayout.setVisibility(View.GONE);
                }
            }
        }
    }

    private void setWind(JSONObject obj, View root) {
        JSONObject wind = null;
        LinearLayout windLayout = root.findViewById(R.id.windLayout);
        try {
            wind = obj.getJSONObject("wind");
        } catch (JSONException e) {
            e.printStackTrace();
            windLayout.setVisibility(View.GONE);
        }

        if (wind != null) {
            boolean visible = true;
            try {
                double speed = wind.getDouble("speed");
                String windText = speed + " m/s";
                TextView windSpeed = root.findViewById(R.id.windSpeed);
                windSpeed.setText(windText);
                root.findViewById(R.id.windSpeedRow).setVisibility(View.VISIBLE);
                windLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.windSpeedRow).setVisibility(View.GONE);
                windLayout.setVisibility(View.GONE);
                visible = false;
            }

            try {
                double windDegrees = wind.getInt("deg");
                String direction = getWindDirection(windDegrees);
                TextView windDirection = root.findViewById(R.id.windDirection);
                windDirection.setText(direction);
                root.findViewById(R.id.windDirectionRow).setVisibility(View.VISIBLE);
                windLayout.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
                root.findViewById(R.id.windDirectionRow).setVisibility(View.GONE);
                if (!visible) {
                    windLayout.setVisibility(View.GONE);
                }
            }
        }
    }

    public static String getWindDirection(double windDegrees) {
        if (windDegrees == 0 || windDegrees == 360) {
            return "North";
        }
        if (windDegrees > 0 && windDegrees < 90) {
            return "North east";
        }
        if (windDegrees == 90) {
            return "East";
        }
        if (windDegrees > 90 && windDegrees < 180) {
            return "South east";
        }
        if (windDegrees == 180) {
            return "South";
        }
        if (windDegrees > 180 && windDegrees < 270) {
            return "South west";
        }
        if (windDegrees == 270) {
            return "West";
        }
        if (windDegrees > 270 && windDegrees < 360) {
            return "North west";
        }

        return "No wind";
    }

    public static String getDateTime(long timestamp, String format) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("Europe/Prague"));
        cal.setTimeInMillis(timestamp * 1000);
        return DateFormat.format(format, cal).toString();
    }

    private void getSelectedCity(View root) {
        CityFragment cityFragment = CityFragment.newInstance(1);
        cityFragment.setSelectedCity(root);
        selectedCity = cityFragment.getSelectedCity();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_today, container, false);

        ImageView image = root.findViewById(R.id.icon);
        image.setImageResource(R.drawable.img_10d);

        setCity(root);

        setStoredData(root);

        controlUpdate(root);

        handleUpdate(root);

        return root;
    }

    private void setCity(View root) {
        getSelectedCity(root);

        TextView city = root.findViewById(R.id.city);

        city.setText(selectedCity.second.toString());
    }

    private void setStoredData(final View root) {

        if (storedData != null && storedData.length() > 0){
            setTodayWeather(storedData, root);
            return;
        }

        JSONObject jsonObject = new JSONObject();

        File file = new File(getContext().getFilesDir(), STORED_TODAY);
        if (!file.exists()) {
            try {
                file.createNewFile();
                try (FileOutputStream fos = getContext().openFileOutput(STORED_TODAY, Context.MODE_PRIVATE)) {
                    fos.write(("{}").getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileInputStream fis;
        try {
            fis = Objects.requireNonNull(getContext()).openFileInput(STORED_TODAY);
            InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8);

            StringBuilder stringBuilder = new StringBuilder();

            try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                String line = reader.readLine();
                while (line != null) {
                    stringBuilder.append(line).append('\n');
                    line = reader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                String contents = stringBuilder.toString();

                JSONObject obj = new JSONObject(contents);

                if (obj.length() > 0) {
                    storedData = obj;
                    setTodayWeather(obj, root);
                }
                else {
                    reloadDataFromServer(root);
                }

                fis.close();
            }
        } catch (JSONException | IOException ex) {
            ex.printStackTrace();
        }
    }

    private void controlUpdate(View root) {
        long time = new Date().getTime();
        File file = new File(root.getContext().getFilesDir(), LAST_UPDATE + selectedCity.first);
        if (!file.exists()) {
            reloadDataFromServer(root);
        } else {
            FileInputStream fis;
            try {
                fis = root.getContext().openFileInput(LAST_UPDATE + selectedCity.first);
                InputStreamReader inputStreamReader = new InputStreamReader(fis, StandardCharsets.UTF_8);

                StringBuilder stringBuilder = new StringBuilder();

                try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                    String line = reader.readLine();
                    while (line != null) {
                        stringBuilder.append(line).append('\n');
                        line = reader.readLine();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    String contents = stringBuilder.toString();

                    JSONObject obj = new JSONObject(contents);

                    if (obj.length() > 0) {
                        int city = obj.getInt("city");
                        time = obj.getLong("date");
                        long now = (new Date()).getTime();

                        if (city != (int)selectedCity.first || now - time > UPDATE_DIFF) {
                            reloadDataFromServer(root);
                            time = new Date().getTime();
                        }
                    }
                    else {
                        reloadDataFromServer(root);
                    }

                    fis.close();
                }
            } catch (JSONException e) {
                reloadDataFromServer(root);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        TextView update = root.findViewById(R.id.updateText);
        String date = DateFormat.format("dd.MM.yyyy HH:mm", new Date(time)).toString();
        String updateText = "Last updated: " + date;
        update.setText(updateText);
    }

    private void handleUpdate(final View root) {

        ImageButton update = root.findViewById(R.id.update);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reloadDataFromServer(root);
                long time = new Date().getTime();
                TextView update = root.findViewById(R.id.updateText);
                String date = DateFormat.format("dd.MM.yyyy HH:mm", new Date(time)).toString();
                String updateText = "Last updated: " + date;
                update.setText(updateText);
            }
        });
    }
}
