package cz.utb.ojanota.weather;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import cz.utb.ojanota.weather.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    public static final String WEATHER_API_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String WEATHER_API_APPID = "74a6eb54f18101fef275750fa910b9b7";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }
}
