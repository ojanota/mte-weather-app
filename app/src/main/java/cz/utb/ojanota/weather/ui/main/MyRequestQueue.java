package cz.utb.ojanota.weather.ui.main;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import androidx.fragment.app.Fragment;

public class MyRequestQueue {
    private static RequestQueue queue = null;

    public static RequestQueue getQueue(Fragment fragment) {
        if (queue == null) {
            return Volley.newRequestQueue(fragment.getContext());
        }

        return queue;
    }
}
